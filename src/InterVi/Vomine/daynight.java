package InterVi.Vomine;

import org.bukkit.event.Listener;
import org.bukkit.ChatColor;
import org.bukkit.World;

import java.util.Timer;
import java.util.TimerTask;

public class daynight implements Listener {
	Vomine main;
	daynight(Vomine all) {main = all; main.dn = this; start(); main.log.info("daynight актикирован");};
	
	Timer timer = new Timer();
	
	public void start() { //старт таймера
		this.timer = new Timer();
		timer.schedule(new tim(), 2000, 2000);
	}
	
	public void stop() { //остановка
		timer.cancel();
	}
	
	private class tim extends TimerTask {
		int ch = 0;
	public void run() {
			World w = main.getW(main.conf.world);
			if (w != null) {
			//23000 - утро
			//13500 - вечер
			long t = w.getTime();
			//включение сохранения инвентаря утром
			if (t >= main.conf.dut | t < main.conf.dve && this.ch == 2 | this.ch == 0 && main.conf.daynight == true) {
				if (!w.setGameRuleValue("keepInventory", "true")) main.log.info("не удалось изменить game rule");
				main.sendAll(ChatColor.translateAlternateColorCodes('&', main.conf.son));
				main.log.info("сохранение инвентаря включено до ночи");
				this.ch = 1;
			}
			//выключение вечером
			if (t >= main.conf.dve & t < main.conf.dut && this.ch == 1 | this.ch == 0 && main.conf.daynight == true) {
				if (!w.setGameRuleValue("keepInventory", "false")) main.log.info("не удалось изменить game rule");
				main.sendAll(ChatColor.translateAlternateColorCodes('&', main.conf.soff));
				main.log.info("сохранение инвентаря выключено до утра");
				this.ch = 2;
			}
			}
	}
	}
	
}