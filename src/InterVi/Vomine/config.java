package InterVi.Vomine;

public class config {
	config() {load();}
	
	//вместо конфига
	boolean daynight = false; //днем вещи не выпадают
		String world = "newworld";
		//23000 - утро
		//13500 - вечер
		int dut = 23000; //утро
		int dve = 13500; //вечер
		String son = "&fсохранение инвентаря включено до ночи"; //сообщение о включении сохранения
		String soff = "&fсохранение инвентаря выключено до утра"; //сообщение о выключении сохранения
	
	boolean nokit = false; //игроки не могут использовать вещи из китов премиумов
		String itemname[] = {"доспех бога"}; //как называются предметы
		int itemreplace = 367; //во что превращать
		String mesclick = "ай ай ай! нельзя так делать"; //сообщение при попытке украсть или переименовать
		String rename = "азазаза"; //во что переименовать
		boolean delete = false; //удалять вместо замены
		boolean anvreplace = false; //заменять предметы на наковальне при попытке переименования
			int anvid = 268; //id замены
			String anvname = "Меч багоюзера"; //название предмета
		boolean killpl = false; //убивать игрока
		boolean kickpl = false; //кикать игрока
			String kickr = "ай ай ай, багоюзер!"; //причина кика
		boolean fastnk = false; //закрывать инвентарь еще при клике в нем (до вставки в слот наковальни)
	
	boolean ref = false; //реферальная система
		boolean refdebug = false; //вывод некоторой отладочной информации
		boolean ipcheck = true; //сверка подсетей
		boolean hostcheck = true; //сверка хостов
			boolean bhostcheck = false; //сверка хостов при поиске в БД
		boolean datacheck = true; //сверка дат (приглашенный меньше на сервере, чем пригласивший)
		boolean newcheck = true; //проверка, что игрок новый
			int newmin = 30; //до скольки минут игрок считается новым (наигранные минуты)
		boolean uuidcheck = true; //сверка UUID-ов
		boolean stat = true; //ведение статистики
		boolean refmess = true; //вывод сообщений в консоль
		boolean offline = false; //пытаться получить информацию, если пригласивший оффлайн
		boolean blacklist = true; //запись нарушений
			int narnum = 3; //кол-во попыток жульничества, при которых происходит блокировка реф. системы для приглашающего
		boolean actwait = true; //задержка перед выдачей приза (приглашенный и пригласивший должны быть онлайн)
			boolean chact = true; //проверка активности приглашенного
				boolean chmove = true; //проверка пройденной дистанции
					int mdist = 100; //расстояние в блоках
				boolean chcommand = true; //проверка выполненных комманд
					int ccom = 1; //количество
					String obcom[] = new String[0]; //обязательные команды (если не нужно - оставить пустым)
				boolean chchat = false; //проверка написанных в чат сообщений
					int cchat = 1; //количество
				boolean chinteract = false; //проверка взаимодействий с блоками
					int iinter = 1; //количество
				boolean chclick = false; //проверка кликов в инвентаре
					int cclick = 1; //количество
				boolean chteleport = false; //проверка телепортаций
					int cctp = 1; //количество
			int wlimit = 200; //лимит на записи в БД для отработки по таймеру
			long waitmin = 5; //время ожидания в минутах
			boolean utpriz = true; //утешительный приз
				boolean utnowait = true; //выдавать ли сразу
				//%ref% - приглашенный, %refer% - пригласивший
				String utcom[] = {"give %refer% iron_ingot 5", "give %ref% iron_ingot 1"}; //команды для выполнения
		boolean refachivment = true; //система достижений
			boolean rachbroadcast = true; //оповещение в чат
			boolean rachtoplay = false; //оповещение игроку
			//массив с отформатированными строками достижений
			//рефералы, лучшие рефералы, нарушения, достижение (выводится в списке), уведомление в чат, уведомление игроку, команды
			//если что-то нужно не учитывать, нужно поставить -1, если нужно не указывать команды - удалить этот блок
			//%refer% - пригласивший, %refnum% - кол-во рефералов, %bestnum% - кол-во лучших рефералов, %narnum% - кол-во нарушений
			String refachset[] = {"10; 5; 0; [Молодец] (пригласить 10 обычных и 5 заинтересованных); &e%refer% пригласил %refnum% обычных и %bestnum% заинтересованных игроков (%narnum% нарушений); &fты выполнил первую ачивку!; give %refer% diamond 10"};
		//если оповещение не нужно - оставить параметр пустым
	    //&c%refer% вышел или не проявлял активности
		String nowait = ""; //если после ожидания проверка провалилась
		//&c%refer% вышел или не проявлял активности. Выдан утешительный приз.
		String utmess = ""; //проверка провалилась и выдан утешительный приз
		//&e%referer% успешно заинтересовал %refer% и получил супер-приз!
		String okwait = ""; //проверка пройдена
		//&eТы был успешно заинтересован %referer%!
		String okwtorefer = ""; //оповещение приглашенному
		//&eТы успешно заинтересовал %refer%!
		String okwtoreferer = ""; //оповещение пригласившему
		//&eПриз получишь через %min% минут, если %refer% заинтересуется. Жди.
		String stwait = ""; //сообщение об ожидании проверки
		//&cДостигнут лимит БД. Довольствуйся утешительным призом...
		String erwait = ""; //если БД переполнена
		//&cЖульничать запрещено!
		String noref = ""; //если была попытка жульничества
		//&cИгрок не найден
		String notfound = ""; //если пригласивший игрок не найден
		//&cДанному игроку заблокирована эта возможность. Доигрался!
		String block = ""; //если пригласившему заблокирована реф. система
		//&cНет прав
		String noperm = ""; //если нет пермишена на какую-либо команду
		//&eТы был успешно приглашен %referer%
		String torefer = ""; //оповещение приглашенному при успешном приглашении (сразу)
		//&eТы успешно пригласил %refer%
		String toreferer = ""; //оповещение пригласившему (сразу)
		//&e%referer% успешно пригласил %refer%!
		String toall = ""; //оповещение в чат
		String refcom[] = {"give %refer% diamond 5", "give %ref% diamond 1"}; //приз (выполнение команд)
		
	boolean points = false; //мини-игра: точки со взрывом и точки с призами
		String prizblock[] = {"give %name% coal 1"}; //команды при попадании в призовой блок
		int psec = 120; //пауза, прежде чем блок повторно выдаст приз игроку
		int psize = 200; //размер памяти
		int chsize = 50; //размер памяти для предотвращения многократного срабатывания
		int chwait = 2; //через сколько секунд точка повторно сработает на игрока
	
	public void load() {
		utils ut = new utils("configLoader");
		String file = ut.getPatch() + "/plugins/Vomine/config.yml";
		ut.saveConfig("/config.yml", file); //сохранение дефолтного конфига, если его нет
		ut.cfload.load(file);
		this.daynight = ut.cfload.getBoolean("daynight");
		this.world = ut.cfload.getString("world");
		this.nokit = ut.cfload.getBoolean("nokit");
		this.itemname = ut.cfload.getStringArray("itemname");
		this.itemreplace = ut.cfload.getInt("itemreplace");
		this.mesclick = ut.cfload.getString("mesclick");
		this.rename = ut.cfload.getString("rename");
		this.delete = ut.cfload.getBoolean("delete");
		this.dut = ut.cfload.getInt("dut");
		this.dve = ut.cfload.getInt("dve");
		this.son = ut.cfload.getString("son");
		this.soff = ut.cfload.getString("soff");
		this.ref = ut.cfload.getBoolean("ref");
		this.ipcheck = ut.cfload.getBoolean("ipcheck");
		this.hostcheck = ut.cfload.getBoolean("hostcheck");
		this.bhostcheck = ut.cfload.getBoolean("bhostcheck");
		this.datacheck = ut.cfload.getBoolean("datacheck");
		this.uuidcheck = ut.cfload.getBoolean("uuidcheck");
		this.stat = ut.cfload.getBoolean("stat");
		this.refmess = ut.cfload.getBoolean("refmess");
		this.offline = ut.cfload.getBoolean("offline");
		this.blacklist = ut.cfload.getBoolean("blacklist");
		this.actwait = ut.cfload.getBoolean("actwait");
		this.chact = ut.cfload.getBoolean("chact");
		this.chmove = ut.cfload.getBoolean("chmove");
		this.chcommand = ut.cfload.getBoolean("chcommand");
		this.chchat = ut.cfload.getBoolean("chchat");
		this.chinteract = ut.cfload.getBoolean("chinteract");
		this.chclick = ut.cfload.getBoolean("chclick");
		this.chteleport = ut.cfload.getBoolean("chteleport");
		this.utpriz = ut.cfload.getBoolean("utpriz");
		this.utnowait = ut.cfload.getBoolean("utnowait");
		this.refachivment = ut.cfload.getBoolean("refachivment");
		this.rachbroadcast = ut.cfload.getBoolean("rachbroadcast");
		this.rachtoplay = ut.cfload.getBoolean("rachtoplay");
		this.refdebug = ut.cfload.getBoolean("refdebug");
		this.newcheck = ut.cfload.getBoolean("newcheck");
		this.narnum = ut.cfload.getInt("narnum");
		this.mdist = ut.cfload.getInt("mdist");
		this.ccom = ut.cfload.getInt("ccom");
		this.cchat = ut.cfload.getInt("cchat");
		this.iinter = ut.cfload.getInt("iinter");
		this.cclick = ut.cfload.getInt("cclick");
		this.cctp = ut.cfload.getInt("cctp");
		this.wlimit = ut.cfload.getInt("wlimit");
		this.newmin = ut.cfload.getInt("newmin");
		this.waitmin = ut.cfload.getLong("waitmin");
		if (ut.cfload.isSet("nowait")) this.nowait = ut.cfload.getString("nowait");
		if (ut.cfload.isSet("utmess")) this.utmess = ut.cfload.getString("utmess");
		if (ut.cfload.isSet("okwait")) this.okwait = ut.cfload.getString("okwait");
		if (ut.cfload.isSet("okwtorefer")) this.okwtorefer = ut.cfload.getString("okwtorefer");
		if (ut.cfload.isSet("okwtoreferer")) this.okwtoreferer = ut.cfload.getString("okwtoreferer");
		if (ut.cfload.isSet("stwait")) this.stwait = ut.cfload.getString("stwait");
		if (ut.cfload.isSet("erwait")) this.erwait = ut.cfload.getString("erwait");
		if (ut.cfload.isSet("noref")) this.noref = ut.cfload.getString("noref");
		if (ut.cfload.isSet("notfound")) this.notfound = ut.cfload.getString("notfound");
		if (ut.cfload.isSet("block")) this.block = ut.cfload.getString("block");
		if (ut.cfload.isSet("noperm")) this.noperm = ut.cfload.getString("noperm");
		if (ut.cfload.isSet("torefer")) this.torefer = ut.cfload.getString("torefer");
		if (ut.cfload.isSet("toreferer")) this.toreferer = ut.cfload.getString("toreferer");
		if (ut.cfload.isSet("toall")) this.toall = ut.cfload.getString("toall");
		this.utcom = ut.cfload.getStringArray("utcom");
		this.refachset = ut.cfload.getStringArray("refachset");
		this.refcom = ut.cfload.getStringArray("refcom");
		if (ut.cfload.isSetArray("obcom")) this.obcom = ut.cfload.getStringArray("obcom");
		this.points = ut.cfload.getBoolean("points");
		this.prizblock = ut.cfload.getStringArray("prizblock");
		this.psec = ut.cfload.getInt("psec");
		this.psize = ut.cfload.getInt("psize");
		this.anvid = ut.cfload.getInt("anvid");
		this.anvname = ut.cfload.getString("anvname");
		this.anvreplace = ut.cfload.getBoolean("anvreplace");
		this.killpl = ut.cfload.getBoolean("killpl");
		this.kickpl = ut.cfload.getBoolean("kickpl");
		this.kickr = ut.cfload.getString("kickr");
		this.fastnk = ut.cfload.getBoolean("fastnk");
		this.chsize = ut.cfload.getInt("chsize");
		this.chwait = ut.cfload.getInt("chwait");
	}
}