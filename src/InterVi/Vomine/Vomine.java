package InterVi.Vomine;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.command.ConsoleCommandSender;

public class Vomine extends JavaPlugin implements Listener {
	class logger { //отправка сообщений в консоль
		private final Logger log = Logger.getLogger("Minecraft");
		public void info(String text) {
				log.info("[Vomine] " + text);
		}
		public void warn(String text) {
			log.warning("[Vomine] " + text);
		}
	}
	logger log = new logger();
	config conf = new config();
	private config conf2 = new config();
	public daynight dn;
	public refer ref;
	public points points;
	private utils ut = new utils("sort");
	
	public World getW(String world) {return getServer().getWorld(world);};
	
	//запуск плагина
	public void onEnable() {
		log.info("---< активация Vomine >---");
		getServer().getPluginManager().registerEvents(new comm(this), this);
		if (conf.daynight == true) getServer().getPluginManager().registerEvents(new daynight(this), this);
		if (conf.nokit == true) getServer().getPluginManager().registerEvents(new antiprem(this), this);
		if (conf.ref == true) getServer().getPluginManager().registerEvents(new refer(this), this);
		if (conf.points == true) getServer().getPluginManager().registerEvents(new points(this), this);
		log.info("---< Vomine активирован >---");
	}
	
	//выключение плагина
	public void onDisable() {
		log.info("---< деактивация Vomine... >---");
		if (dn != null) dn.stop();
		if (conf2.ref && conf2.actwait && ref != null) ref.offTimer();
		log.info("---< Vomine деактивирован >---");
	}
	
	public void sendAll(String mes) { //отправка сообщения всем игрокам
		int psize = Bukkit.getServer().getOnlinePlayers().size();
		Iterator<? extends Player> iter =  Bukkit.getServer().getOnlinePlayers().iterator();
		for (int i = 0; i < psize; i++) {
			iter.next().sendMessage(ChatColor.ITALIC + mes);;
		}
	}
	
	public void reload() { //перезагрузка
		conf.load();
		if (conf2.daynight == false && conf.daynight == true) getServer().getPluginManager().registerEvents(new daynight(this), this);
		if (conf2.nokit == false && conf.nokit == true) getServer().getPluginManager().registerEvents(new antiprem(this), this);
		if (conf2.ref == false && conf.ref == true) getServer().getPluginManager().registerEvents(new refer(this), this);
		if (conf2.points == false && conf.points == true) getServer().getPluginManager().registerEvents(new points(this), this);
	}
	
	//для рефералки
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("ref")) {
			if (sender instanceof Player) {
				if (sender.hasPermission("vomine.ref") | sender.isOp()) { //команда указания реферала
					if (args.length == 1) ref.onRef(((Player) sender).getPlayerListName(), args[0]); else 
						sender.sendMessage("Используй: /ref ник");
					return true;
				} else { //если нет пермишена
					if (conf.noperm.length() > 1) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', conf.noperm));
					return true;
				}
			} else { //если команда от консоли
				sender.sendMessage("эту команду можно выполнять только из игры");
				return true;
			}
		} else if (command.getName().equalsIgnoreCase("ref-admin")) { //обработка команд админа
			/*
			 * info
			 * infodb
			 * stat
			 * block
			 * unlock
			 * refers
			 * clearnar
			 */
			if (sender.hasPermission("vomine.ref.admin") | sender instanceof ConsoleCommandSender) { //если есть пермишен
				if (args != null) {
					if (args.length > 0) { //должны быть указаны параметры, иначе выводим справку
						if (args.length >= 2) { //обработка команд с 2 параметрами
							if (args[0].equalsIgnoreCase("info")) { //информация о игроке
								String info[] = null; if (ref != null) info = ref.checkUser(args[1]);
								if (info != null) {
									for (int i = 0; i < info.length; i++) sender.sendMessage(info[i]);
								} else sender.sendMessage("нет данных");
							} else if (args[0].equalsIgnoreCase("infodb")) { //информация о игроке из БД
								String info[] = null; if (ref != null) info = ref.checkUserInDB(args[1]);
								if (info != null) {
									for (int i = 0; i < info.length; i++) sender.sendMessage(info[i]);
								} else sender.sendMessage("нет данных");
							} else if(args[0].equalsIgnoreCase("block")) { //блокировка игрока
								if (ref != null) {
									if (ref.blockUser(args[1])) sender.sendMessage(args[1] + " заблокирована реферальная система"); else
										sender.sendMessage("ошибка, " + args[1] + " уже заблокирован или не найден");
								} else sender.sendMessage("произошла ошибка");
							} else if (args[0].equalsIgnoreCase("unlock")) { //разблокировка игрока
								if (ref != null) {
									if (ref.unlockUser(args[1])) sender.sendMessage(args[1] + " разблокирована реферальная система"); else
										sender.sendMessage("ошибка, " + args[1] + " уже разблокирован или не найден");
								} else sender.sendMessage("произошла ошибка");
							} else if (args[0].equalsIgnoreCase("clearnar")) { //очистка статистики нарушений игрока
								if (ref != null) {
									if (ref.clearNar(args[1])) sender.sendMessage("У " + args[1] + " очищена статистика нарушений"); else
										sender.sendMessage("ошибка, у " + args[1] + " уже очищены нарушения или игрок не найден");
								} else sender.sendMessage("произошла ошибка");
							} else if (args[0].equalsIgnoreCase("refers")) { //список пригласивших (вывод списка)
								String refers[] = null; if (ref != null) refers = ref.getRefers();
								if (refers != null) {
									if (args.length == 2) {
										int str = -1;
										try {
											str = Integer.parseInt(args[1]);
										} catch (Exception e) {
											ut.sort.sendData(refers);
											ut.sort.go();
											sender.sendMessage("/ref-admin refers страница (страниц: " + String.valueOf(ut.sort.getNumStr()) + ")");
										}
										if (str > -1) {
											ut.sort.sendData(refers);
											ut.sort.go();
											String mess[] = ut.sort.getStr(str-1); //-1 потому что массив начинается с 0
											if (mess != null) {
												sender.sendMessage("---> Пригласившие (стр. " + args[1] + " из " + String.valueOf(ut.sort.getNumStr()) + "):");
												for (int i = 0; i < mess.length; i++) {
													if (mess[i] != null) sender.sendMessage("-> " + mess[i].trim()); else break;
												}
												sender.sendMessage("<--->");
											} else sender.sendMessage("произошла ошибка");
										}
									}
								} else sender.sendMessage("нет данных");
							} else sender.sendMessage("неправильная команда");
						} else { //если параметр 1
							if (args[0].equalsIgnoreCase("stat")) { //общая статистика
								String stat[] = null; if (ref != null) stat = ref.getStat();
								if (stat != null) {
									sender.sendMessage("---> Общая статистика реферальной системы");
									for (int i = 0; i < stat.length; i++) sender.sendMessage(stat[i]);
									sender.sendMessage("<--->");
								} else {
									sender.sendMessage("нет статистики");
								}
							} else if (args[0].equalsIgnoreCase("refers")) { //список всех пригласивших
								String refers[] = null; if (ref != null) refers = ref.getRefers();
								ut.sort.sendData(refers);
								ut.sort.go();
								sender.sendMessage("/ref-admin refers страница (страниц: " + String.valueOf(ut.sort.getNumStr()) + ")");
							} else sender.sendMessage("неправильная команда");
						}
					} else { //вывод справки
						sender.sendMessage("<--- Список команд управления реферальной системой --->");
						sender.sendMessage("/ref-admin info игрок - информация о игроке");
						sender.sendMessage("/ref-admin infodb - информация о игроке из БД");
						sender.sendMessage("/ref-admin stat - статистика использования реферальной системы");
						sender.sendMessage("/ref-admin block игрок - заблокировать игроку реферальную систему");
						sender.sendMessage("/ref-admin unlock игрок - разблокировать игроку реферальную систему");
						sender.sendMessage("/ref-admin refers - весь список пригласивших");
						sender.sendMessage("/ref-admin clearnar игрок - очистить статистику нарушений игрока");
						sender.sendMessage("<--->");
					}
				}
			} else if (conf.noperm.length() > 1) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', conf.noperm));
			return true;
			//костыль
		} else if (command.getName().equalsIgnoreCase("vom") | command.getName().equalsIgnoreCase("vomine") | command.getName().equalsIgnoreCase("prefix") | command.getName().equalsIgnoreCase("delprefix")) {
			return true;
		} else if (command.getName().equalsIgnoreCase("refinfo")) { //показ игроку информации о нем
			if (sender instanceof Player) {
				if (sender.hasPermission("vomine.ref.info")) {
					if (ref != null) {
						String info[] = null; if (ref != null) info = ref.checkUser(((Player) sender).getName());
						if (info != null) {
							for (int i = 0; i < info.length; i++) sender.sendMessage(info[i]);
						} else sender.sendMessage("нет данных");
					} else sender.sendMessage("произошла ошибка");
				} else if (conf.noperm.length() > 1) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', conf.noperm));
			} else sender.sendMessage("эту команду можно выполнять только из игры");
			return true;
		} else
		return false;
	}
	
	//включение-выключение модулей
	PluginManager pm = Bukkit.getPluginManager();
	//daynight
	public void ond() {
		if (conf2.daynight == false) {
			conf.daynight = true;
			pm.registerEvents(new daynight(this), this);
		} else {
			conf.daynight = true;
			if (dn != null) dn.start();
		}
	}
	public void offd() {
		conf.daynight = false;
		if (dn != null) dn.stop();
	}
	//antiprem (nokit)
	public void onNokit() {
		if (!conf2.nokit) {
			conf.nokit = true;
			pm.registerEvents(new antiprem(this), this);
		} else conf.nokit = true;
	}
	public void offNokit() {
		conf.nokit = false;
	}
	//рефералка
	public void onRef() {
		if (!conf2.ref) {
			conf.ref = true;
			pm.registerEvents(new antiprem(this), this);
		} else conf.ref = true;
	}
	public void offRef() {
		conf.ref = false;
	}
	//мини-игра points
	public void onPoints() {
		if (!conf2.points) {
			conf.points = true;
			pm.registerEvents(new points(this), this);
		} else conf.points = true;
	}
	public void offPoints() {
		conf.points = false;
	}
}