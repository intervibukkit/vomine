package InterVi.Vomine;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Date;
import java.util.ArrayList;
import java.io.File;
import java.util.Random;

import InterVi.Vomine.utils.count;

public class points implements Listener {
	private Vomine main;
	private String ug[] = {"FileDataBase", "count"};
	private utils ut = new utils(ug);
	
	points(Vomine m) {
		main = m;
		File f = new File(ut.getPatch() + "/plugins/Vomine/points.yml");
		if (f.isFile()) {
			ut.fdb.read(ut.getPatch() + "/plugins/Vomine/points.yml");
		}
		main.points = this;
		main.log.info("модуль пиро-точек активирован");
	}
	
	void addPoint(Player player, boolean boom, float power, boolean com, String command) { //добавить точку в БД
		Location loc = player.getLocation();
		/*
		 * 0 world
		 * 1 x
		 * 2 y
		 * 3 z
		 * 4 boom
		 * 5 power
		 * 6 com
		 * 7 command (com1,com2,com3...)
		 */
		String coords = loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ() + ";" + boom + ";" + power + ";" + com + ";" + command;
		ut.fdb.base.add(coords);
		ut.fdb.write(ut.getPatch() + "/plugins/Vomine/points.yml");
	}
	
	boolean delPoint(Player player) { //удалить точку
		boolean result = false;
		Location loc = player.getLocation();
		String coords = loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ();
		for (int i = 0; i < ut.fdb.base.size(); i++) {
			if (ut.fdb.base.get(i).indexOf(coords) > -1) {
				ut.fdb.base.remove(i);
				ut.fdb.write(ut.getPatch() + "/plugins/Vomine/points.yml");
				result = true;
				break;
			}
		}
		return result;
	}
	
	void delAll() { //удалить все точки
		ut.fdb.base.clear();
		File f = new File(ut.getPatch() + "/plugins/Vomine/points.yml");
		f.delete();
	}
	
	private ArrayList<String[]> db = new ArrayList<String[]>();
	
	private void addInChecker(Player player, Location loc) { //добавить точку и игрока в список проверки на таймер
		String add[] = {
				player.getName(),
				String.valueOf(new Date().getTime()),
				(loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ())
		};
		for (int i = 0; i < db.size(); i++) {
			if (db.get(i)[0].equals(player.getName())) { //чистим дубли
				db.remove(i);
				break;
			}
		}
		if (db.size() <= main.conf.psize) db.add(add); else {
			if (ut.count.get() <= main.conf.psize) {
				db.set(ut.count.get(), add);
				ut.count.next();
			} else {
				ut.count.reset();
				db.set(ut.count.get(), add);
				ut.count.next();
			}
		}
	}
	
	private boolean isAllowed(String name, Location loc) { //проверка разрешения выдачи приза
		boolean result = true;
		String coords = loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ();
		long stamp = new Date().getTime();
		for (int i = 0; i < db.size(); i++) {
			if (db.get(i)[0].equals(name) && db.get(i)[2].equals(coords)) {
				long oldstamp = 0;
				try {
					oldstamp = Long.parseLong(db.get(i)[1].trim());
				} catch (Exception e) {e.printStackTrace();}
				if (((stamp / 1000) - (oldstamp / 1000)) < main.conf.psec) result = false;
				break;
			}
		}
		return result;
	}
	
	private ArrayList<Player> seek = new ArrayList<Player>();
	
	boolean seekSwitch(Player player) { //переключить режим поиска точек
		/*
		 * true - режим включен
		 * false - выключен
		 */
		if (!seek.contains(player)) {
			seek.add(player);
			return true;
		} else {
			seek.remove(player);
			return false;
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onQuitGame(PlayerQuitEvent event) { //очистка списка от вышедших null
		if (main.conf.points) {
			Player player = event.getPlayer();
			if (seek.contains(player)) seek.remove(player);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onMove(PlayerMoveEvent event) { //обработка перемещений
		if (main.conf.points) {
			pointCheck(event.getPlayer(), event.getTo(), event.getFrom(), event.isCancelled());
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onTp(PlayerTeleportEvent event) { //обработка телепортаций
		if (main.conf.points) {
			pointCheck(event.getPlayer(), event.getTo(), event.getFrom(), event.isCancelled());
		}
	}
	
	private ArrayList<long[]> ch = new ArrayList<long[]>();
	
	private count co = ut.count.getCount();
	private void addCh(int hash, int hashloc) { //добавить на проверку
		long date = new Date().getTime();
		long add[] = {hash, date, hashloc};
		if (co.get() <= main.conf.chsize) ch.add(add); else {
			if (co.get() <= main.conf.chsize) {
				ch.set(co.get(), add);
				co.next();
			} else {
				co.reset();
				ch.set(co.get(), add);
				co.next();
			}
		}
	}
	
	private boolean isCh(int hash, int hashloc) { //проверка на время с предыдущего прохождения точки
		boolean result = false;
		for (int i = 0; i < ch.size(); i++) {
			if (ch.get(i)[0] == ((long) hash) && ch.get(i)[2] == ((long) hashloc)) {
				if ((((new Date().getTime()) / 1000) - (ch.get(i)[1] / 1000)) < main.conf.chwait) {
					result = true;
					break;
				}
			}
		}
		return result;
	}
	
	private void pointCheck(Player player, Location to, Location from, boolean cancel) { //обработка событий
		if (ut.fdb.base.isEmpty()) return;
		if (player.hasPermission("vomine.points.use") && !cancel) {
			if (to.getBlockX() != from.getBlockX() | to.getBlockZ() != from.getBlockZ() | to.getBlockY() != from.getBlockY()) { //если игрок наступает на новый блок
				String coords = to.getWorld().getName() + ";" + to.getBlockX() + ";" + to.getBlockY() + ";" + to.getBlockZ();
				int p = -1;
				for (int i = 0; i < ut.fdb.base.size(); i++) { //поиск по БД
					if (ut.fdb.base.get(i).indexOf(coords) > -1) {
						p = i;
						break;
					}
				}
				if (p > -1 && !isCh(player.getName().hashCode(), coords.hashCode())) { //если найдено совпадение
					//обработка режима поиска
					if (seek.contains(player)) {
						if (ut.fdb.base.get(p).split(";")[4].equals("true")) {
							player.sendMessage("[Vomine Points] это взрывная точка, сила " + ut.fdb.base.get(p).split(";")[5]);
						} else if (ut.fdb.base.get(p).split(";")[6].equals("false")) {
							player.sendMessage("[Vomine Points] это призовая точка");
						} else {
							player.sendMessage("[Vomine Points] это командная точка, команда(ы): " + ut.fdb.base.get(p).split(";")[7]);
						}
						addCh(player.getName().hashCode(), coords.hashCode());
						return;
					}
					//обработка срабатывания точек
					if (ut.fdb.base.get(p).split(";")[4].equals("true")) { //если это взрывная точка
						float power = 0;
						try {
							power = Float.parseFloat(ut.fdb.base.get(p).split(";")[5]);
						} catch (Exception e) {e.printStackTrace();}
						to.getWorld().createExplosion(to.getX(), to.getY(), to.getZ(), power, false, false);
						addCh(player.getName().hashCode(), coords.hashCode());
						return;
					} else if (ut.fdb.base.get(p).split(";")[6].equals("false")) { //если призовая
						if (isAllowed(player.getName(), to)) { //проверка разрешения
							Random rand = new Random();
							rand.setSeed(rand.nextLong());
							int ver = rand.nextInt(main.conf.prizblock.length);
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.prizblock[ver].replaceAll("%name%", player.getName()));
							addInChecker(player, to);
							addCh(player.getName().hashCode(), coords.hashCode());
							return;
						}
					} else { //если коммандная
						if (ut.fdb.base.get(p).split(";")[7].equals("null")) return;
						String commands[] = ut.fdb.base.get(p).split(";")[7].split(",");
						if (commands.length > 0) {
							for (int n = 0; n < commands.length; n++) Bukkit.dispatchCommand(Bukkit.getConsoleSender(), commands[n].trim().replaceAll("%name%", player.getName()));
						}
						addCh(player.getName().hashCode(), coords.hashCode());
					}
				}
			}
		}
	}
}