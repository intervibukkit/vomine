package InterVi.Vomine;

import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class antiprem implements Listener {
	Vomine main;
	antiprem(Vomine all) {main = all; main.log.info("анти-кит активирован");}
	private utils ut = new utils();
	
	@EventHandler(priority = EventPriority.LOW)
	public void onOpenInventory(InventoryOpenEvent event) { //поиск и замена итемов в инвентаре при его открытии
		Player player = (Player) event.getPlayer();
		if (main.conf.nokit && player != null && !player.hasPermission("vomine.premkit") && !player.isOp()) { //если нет пермишена - проверяем
			ItemStack[] inv = player.getInventory().getContents();
			ItemStack replace = new ItemStack(main.conf.itemreplace);
			ItemMeta meta = replace.getItemMeta();
			meta.setDisplayName(main.conf.rename);
			replace.setItemMeta(meta);
			for (int i = 0; i < inv.length; i++) {
				if (inv[i] != null && inv[i].getItemMeta().hasDisplayName()) {
					//если предмет с заданным названием нашелся
					if (ut.chW(ut.cleform(inv[i].getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
						//удаляем или заменяем
						if (main.conf.delete) player.getInventory().remove(inv[i]); else player.getInventory().setItem(i, replace);
					}
				}
			}
			inv = player.getInventory().getArmorContents();
			for (int i = 0; i < inv.length; i++) {
				if (inv[i] != null && inv[i].hasItemMeta()) {
					if (inv[i].getItemMeta().hasDisplayName()) {
					//если предмет с заданным названием нашелся
					if (ut.chW(ut.cleform(inv[i].getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
						//удаляем или заменяем
						if (main.conf.delete) player.getInventory().remove(inv[i]); else player.getInventory().setItem(i, replace);
					}
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onClickInventory(InventoryClickEvent event) { //поиск и замена итемов в инвентаре при клике в нем
		Player player = (Player) event.getWhoClicked();
		boolean send = false; //чтобы сообщение не отправлялось 2 раза
		if (main.conf.nokit && player != null) { //если нет пермишена - проверяем
			ItemStack replace = new ItemStack(main.conf.itemreplace);
			ItemMeta meta = replace.getItemMeta();
			meta.setDisplayName(main.conf.rename);
			replace.setItemMeta(meta);
			ItemStack click = event.getCurrentItem();
			ItemStack cursor = event.getCursor();
			String type = event.getSlotType().toString();
			if (!player.hasPermission("vomine.premkit") && !player.isOp() && click != null) {
			if (click.getItemMeta() != null && type.equals("CONTAINER") | type.equals("ARMOR")) {
				ItemStack[] inv = player.getInventory().getContents();
				for (int i = 0; i < inv.length; i++) {
					if (inv[i] != null && inv[i].getItemMeta().hasDisplayName()) {
						//если предмет с заданным названием нашелся
						if (ut.chW(ut.cleform(inv[i].getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
							//удаляем или заменяем его
							if (main.conf.delete) event.getWhoClicked().getInventory().remove(inv[i]); else event.getWhoClicked().getInventory().setItem(i, replace);
						}
					}
				}
				inv = player.getInventory().getArmorContents();
				for (int i = 0; i < inv.length; i++) {
					if (inv[i] != null && inv[i].hasItemMeta()) {
						if (inv[i].getItemMeta().hasDisplayName()) {
						//если предмет с заданным названием нашелся
						if (ut.chW(ut.cleform(inv[i].getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
							//удаляем или заменяем
							if (main.conf.delete) player.getInventory().remove(inv[i]); else player.getInventory().setItem(i, replace);
						}
						}
					}
				}
				//блокировка воровства
				if (click != null && click.getItemMeta() != null) {
					if (click.getItemMeta().hasDisplayName()) {
					if (ut.chW(ut.cleform(click.getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
						event.setCancelled(true);
						if (!send) {player.sendMessage(main.conf.mesclick); send = true;}
					}
				}}
			}}
			if (!player.isOp() && click != null) { //блокировка переименования В
			if (click.getItemMeta() != null) {
				if (click.getItemMeta().hasDisplayName() && type.equals("RESULT")) {
					if (ut.chW(ut.cleform(click.getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname))) {
						event.setCancelled(true);
						player.closeInventory();
						if (!send) {player.sendMessage(main.conf.mesclick); send = true;}
					}
				}
			}}
			if (!player.isOp() && cursor != null) { //блокировка переименования ИЗ
			if (cursor.getItemMeta() != null && cursor.getItemMeta().hasDisplayName()) {
				if (ut.chW(ut.cleform(cursor.getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname)) && type.equals("CRAFTING") && event.getInventory().getSize() == 3) {
					event.setCancelled(true);
					player.closeInventory();
					if (main.conf.anvreplace) {
						ItemStack item = new ItemStack(main.conf.anvid, 1);
						ItemMeta imeta = item.getItemMeta(); imeta.setDisplayName(main.conf.anvname);
						item.setItemMeta(imeta);
						event.setCursor(new ItemStack(0));
						event.setCurrentItem(item);
					}
					if (!send) {player.sendMessage(main.conf.mesclick); send = true;}
					if (main.conf.killpl) player.setHealth(0.0D);
					if (main.conf.kickpl) player.kickPlayer(main.conf.kickr);
				}
			}}
			if (!player.isOp() && click != null) { //быстрая блокировка, до перемещения предмета в слот наковальни
				if (main.conf.fastnk) {
					if (click.hasItemMeta()) {
					if (click.getItemMeta().hasDisplayName()) {
					if (ut.chW(ut.cleform(click.getItemMeta().getDisplayName().toLowerCase().trim()), ut.mlcase(main.conf.itemname)) && event.getView() != null) {
						if (event.getView().getTopInventory() != null) {
							if (event.getView().getTopInventory().getType().toString().equals("ANVIL")) {
								event.setCancelled(true);
								event.getView().close();
								player.closeInventory();
								if (!send) {player.sendMessage(main.conf.mesclick); send = true;}
							}
						}
					}
					}
					}
				}
			}
		}
	}
	
}