package InterVi.Vomine;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.server.RemoteServerCommandEvent;
import org.bukkit.Bukkit;

public class comm implements Listener {
	Vomine main;
	private utils ut = new utils();
	comm(Vomine all) {main = all; main.log.info("модуль обработки команд активирован");};
	
	//команда из консоли
	@EventHandler(priority = EventPriority.LOWEST)
	public void sendCom(ServerCommandEvent event) {
		this.cons = true;
		String com = event.getCommand();
		if (checkCom(com)) {
			this.noedit = com;
			go(com);
		}
	}
		
	//команда из игры
	@EventHandler(priority = EventPriority.HIGHEST)
	public void sendComingame(PlayerCommandPreprocessEvent event) {
		this.cons = false;
		String com = event.getMessage();
		if (checkCom(com) && !event.isCancelled()) {
			this.noedit = com;
			this.player = event.getPlayer();
			go(com);
		}
	}
	
	//команда из Rcon
	@EventHandler(priority = EventPriority.LOWEST)
	public void sendCominRcon(RemoteServerCommandEvent event) {
		this.cons = true;
		String com = event.getCommand();
		if (checkCom(com)) {
			this.noedit = com;
			go(com);
		}
	}
	
	private boolean cons; //определяет, куда отправлять сообщения
	private String noedit; //не отредактированная команда
	private Player player; //игрок, которому отправлять сообщения
	
	private void toCons(String mes) {main.log.info(mes);} //отправка сообщений в консоль
	private void toGame(String mes) {if (player != null) player.sendMessage(mes);} //отправка сообщений игроку
	private void toOut(String mes) {if (this.cons == true) toCons(mes); else if (this.cons == false) toGame(mes);} //отправить сообщение (сам определит куда)
	
	private boolean checkCom(String com) { //проверка, относится ли команда к данному плагину
		String check[] = {"vomine", "vom", "prefix"};
		boolean result = false;
		com = com.toLowerCase();
		if (this.cons == false) com = ut.remChars(com, 0, 1);
		for (int i = 0; i < check.length; i++) {
			if (ut.remChars(com, check[i].length(), com.length()).equals(check[i])) {result = true; break;} else {result = false;}
		}
		return result;
	}
	
	private boolean checkPerm(String com, Player p) { //проверка разрешений на выполнение команды
		boolean result = false;
		if (p != null) {
			if (com.equals("help") | com.equals("vom") | com.equals("vomine") && p.hasPermission("vomine.help") | p.hasPermission("vomine.admin")) result = true;
			if (com.equals("dn on") | com.equals("dn off") && p.hasPermission("vomine.admin")) result = true;
			if (com.equals("nokit on") | com.equals("nokit off") && p.hasPermission("vomine.admin")) result = true;
			if (com.equals("reload") | com.equals("reload") && p.hasPermission("vomine.admin")) result = true;
			if (ut.remChars(com, com.indexOf(" "), com.length()).equals("prefix") && p.hasPermission("vomine.prefix") | p.hasPermission("vomine.admin")) result = true;
			if (com.equals("delprefix") && p.hasPermission("vomine.prefix")) result = true;
			if (ut.remChars(com, 9, com.length()).trim().equals("point add") && p.hasPermission("vomine.points.set") | p.hasPermission("vomine.admin")) result = true;
			if (com.equals("point remove") | com.equals("point seek") && p.hasPermission("vomine.points.set") | p.hasPermission("vomine.admin")) result = true;
			if (com.equals("points on") | com.equals("points off") && p.hasPermission("vomine.admin")) result = true;
			//if (p.isOp()) result = true;
		}
		return result;
	}
	
	private String comClear(String com) { //чистка от лишнего (название плагина, слэш)
		if (this.cons == true) {
			com = com.toLowerCase();
			if (com.length() >= 8) {if (ut.remChars(com, 6, com.length()).equals("vomine")) com = ut.remChars(com, 0, 7);}
			if (com.length() >= 5) {if (ut.remChars(com, 3, com.length()).equals("vom")) com = ut.remChars(com, 0, 4);};
		} else {
			com = com.toLowerCase();
			com = ut.remChars(com, 0, 1);
			if (com.length() >= 8) {if (ut.remChars(com, 6, com.length()).equals("vomine")) com = ut.remChars(com, 0, 7);}
			if (com.length() >= 5) {if (ut.remChars(com, 3, com.length()).equals("vom")) com = ut.remChars(com, 0, 4);};
		}
		return com;
	}
	
	private void go(String com) { //пердобработка команды (чистка и проверка)
		if (this.cons == true) {
			if (comProc(comClear(com)) == false) toOut("нет такой команды, почитай /vom help");
		} else {
			if (player != null) {
				if (checkPerm(comClear(com), this.player)) {if (comProc(comClear(com)) == false) toOut("нет такой команды, почитай /vom help");} else toOut("нет прав");
			}
		}
	}
	
	private boolean comProc(String com) { //обработчик команд
		if (ut.remChars(com, com.indexOf(" "), com.length()).equals("prefix") | ut.remChars(com, com.indexOf(" "), com.length()).equals("/prefix")) com = "prefix";
		//для points
		float power = 0;
		String coms = "null";
		if (com.indexOf("point add boom") > -1) {
			String args[] = com.split(" ");
			if (args.length >= 3) {
				float pw = 0;
				try {
					pw = Float.parseFloat(args[3].trim());
				} catch (Exception e) {}
				power = pw;
				com = "point add boom";
			}
		}
		if (com.indexOf("point add com") > -1) {
			String args[] = com.split(" ");
			if (args.length >= 3) {
				coms = com.substring(13, com.length()).trim();
				com = "point add com";
			}
		}
		
		if (com.equals("vom") | com.equals("vomine")) {toOut("/vom help - справка"); return true;} else
		if (com.equals("help") | com.equals("help 1")) {
			toOut("---< Vomine >---");
			toOut("/vom reload - перезагрузить плагин");
			toOut("/vom dn on/off - включить сохранение инвентаря только днем");
			toOut("/vom ref on/off - включить реферальную систему");
			toOut("/vom nokit on/off - включить блокировку Premium вещей у игроков");
			toOut("/prefix префикс - сменить префикс");
			toOut("/delprefix - удалить префикс");
			toOut("/ref-admin - команды админа реферальной системы");
			toOut("/refinfo - информация о себе в реферальной системе");
			toOut("/ref игрок - указать реферала");
			toOut("/vom point add boom power(1-...) - добавить точку взрыва");
			toOut("/vom point add priz - добавить призовую точку");
			toOut("/vom point add com com1,com2 - добавить коммандную точку");
			toOut("/vom point remove - удалить точку");
			toOut("/vom point clear - удалить все точки");
			toOut("/vom point seek - переключить режим поиска точек");
			toOut("/vom points on/off - включить модуль мини-игры");
			toOut("---- Created by InterVi ----");
			return true;
		} else
		if (com.equals("dn on")) {
			main.ond();
			toOut("daynight включен");
			return true;
		} else
		if (com.equals("dn off")) {
			main.offd();
			toOut("daynight выключен");
			return true;
		} else
		if (com.equals("nokit on")) {
			main.onNokit();
			toOut("блокировка Premium вещей у игроков включена");
			return true;
		} else
		if (com.equals("nokit off")) {
			main.offNokit();
			toOut("блокировка Premium вещей у игроков выключена");
			return true;
		} else
		if (com.equals("reload")) {
			main.reload();
			toOut("конфиг перезагружен");
			return true;
		} else
		if (com.equals("prefix")) {
			if (!cons) {
				if (noedit.indexOf(" ") != -1) {
					String prefix = ut.remChars(noedit, 0, noedit.indexOf(" ")+1);
					int ch = prefix.indexOf('"');
					int ch2 = prefix.lastIndexOf('"');
					if (ch == -1 | ch2 == -1 && ch2 <= ch) {
						prefix = '"' + prefix + " " + '"';
					}
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + ut.cleform(player.getPlayerListName()) + " prefix " + prefix);
					toOut("префикс установлен");
				} else toOut("префикс не задан");
			} else toOut("эту команду можно выполнять только из игры!");
			return true;
		}
		if (com.equals("delprefix")) {
			if (!cons) {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "pex user " + ut.cleform(player.getPlayerListName()) + " prefix " + '"' + '"');
				toOut("префикс удален");
			} else toOut("эту команду можно выполнять только из игры!");
			return true;
		} else
		if (com.equals("ref on")) {
			main.onRef();
			toOut("реферальная система включена");
			return true;
		} else
		if (com.equals("ref off")) {
			main.offRef();
			toOut("реферальная система выключена");
			return true;
		} else
		if (com.equals("points on")) {
			main.onPoints();
			toOut("мини-игра точки включена");
			return true;
		} else
		if (com.equals("points off")) {
			main.offPoints();
			toOut("мини-игра точки выключена");
			return true;
		} else
		if (com.equals("point remove")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				if (cons) {
					toOut("эту команду можно выполнять только из игры");
					return true;
				}
				if (main.points.delPoint(player)) toOut("точка удалена"); else toOut("на данной позиции точка не найдена");
			} else toOut("ошибка");
			return true;
		} else
		if (com.equals("point clear")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				main.points.delAll();
				toOut("все точки удалены");
			} else toOut("ошибка");
			return true;
		} else
		if (com.equals("point add priz")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				if (cons) {
					toOut("эту команду можно выполнять только из игры");
					return true;
				}
				main.points.addPoint(player, false, 0, false, "null");
				toOut("призовая точка создана");
			} else toOut("ошибка");
			return true;
		} else
		if (com.equals("point add boom")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				if (cons) {
					toOut("эту команду можно выполнять только из игры");
					return true;
				}
				if (power > 0) {
					main.points.addPoint(player, true, power, false, "null");
					toOut("взрывная точка создана");
				} else {
					toOut("Ошибка синтаксиса команды!");
					toOut("/vom point add boom power(1-...)");
				}
			} else toOut("ошибка");
			return true;
		} else
		if (com.equals("point add com")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				if (cons) {
					toOut("эту команду можно выполнять только из игры");
					return true;
				}
				if (!coms.equals("null")) {
					main.points.addPoint(player, false, 0, true, coms);
					toOut("командная точка создана");
				} else {
					toOut("Ошибка синтаксиса команды!");
					toOut("/vom point add com com1,com2");
				}
			} else toOut("ошибка");
			return true;
		} else
		if (com.equals("point seek")) {
			if (!main.conf.points) {toOut("модуль выключен"); return true;}
			if (main.points != null) {
				if (cons) {
					toOut("эту команду можно выполнять только из игры");
					return true;
				}
				if (main.points.seekSwitch(player)) toOut("режим поиска точек включен для вас"); else toOut("режим поиска точек выключен для вас");
			} else toOut("ошибка");
			return true;
		} else
		return false;
	}
	
}