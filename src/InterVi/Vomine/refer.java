package InterVi.Vomine;

import org.bukkit.event.Listener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.io.File;
import java.lang.Thread;
import java.util.Date;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Timer;
import java.util.TimerTask;

import InterVi.Vomine.utils.configLoader;
import InterVi.Vomine.utils.configWriter;

public class refer implements Listener {
	private String init[] = {"FileDataBase", "configLoader", "configWriter"};
	private utils ut = new utils(init);
	private Vomine main;
	refer(Vomine v) {
		main = v;
		main.ref = this;
		if (main.conf.actwait) onTimer();
		main.log.info("реферальная система активирована");
	}
	
	/*
	 * 0 - ник
	 * 1 - таймштамп
	 * 2 - данные (опция:значение;опция...)
	 * реферал,рефер/таймштамп/данные
	 */
	private ArrayList<String> waits = new ArrayList<String>(); //БД с ожидающими выдачи основного приза игроками
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userQuit(PlayerQuitEvent event) {
		if (main.conf.ref & main.conf.actwait && !waits.isEmpty()) { //удаление из БД вышедших игроков (тех, которые пригласили)
			String name = event.getPlayer().getName();
			for (int i = 0; i < waits.size(); i++) {
				if (waits.get(i).split("/")[0].split(",")[1].equals(name)) {
					waits.remove(i);
					break;
				}
			}
		}
	}
	
	private void optPlus(String name, String option, double add, String ad) { //изменение числовой опции в БД
		for (int i = 0; i < waits.size(); i++) {
			if (waits.get(i).split("/")[0].split(",")[1].equals(name) && waits.get(i).split("/").length == 3) { //если есть часть с данными
				String options = waits.get(i).split("/")[2];
				if (options.indexOf(";") > 0) { //если прописано несколько опций
				String opt[] = options.split(";");
				for (int n = 0; n < opt.length; n++) { //поиск опции
					if (opt[n].split(":")[0].equals(option)) { //если найдена - добавляем значение
						if (ad == null) {
							double old = 0;
							try {
								old = Double.parseDouble(opt[n].split(":")[1]);
							} catch (Exception e) {e.printStackTrace();}
							opt[n] = (option + ":" + String.valueOf((Math.ceil((old+add)*100)/100)));
						} else opt[n] = opt[n] + "," + ad;
						options = opt[0];
						for (int u = 1; u < opt.length; u++) options += ";" + opt[u]; //пересборка строки
						waits.set(i, (waits.get(i).substring(0, waits.get(i).lastIndexOf("/")) + "/" + options)); //заменяем строку в БД
						break;
					} else if (n == opt.length-1) { //если опция не найдена - добавляем ее
						if (ad == null) options += ";" + option + ":" + String.valueOf(add); else options += ";" + option + ":" + ad;
						waits.set(i, (waits.get(i).substring(0, waits.get(i).lastIndexOf("/")) + "/" + options)); //пересборка строки в БД
					}
				}
				} else { //если опция только одна
					if (options.indexOf(":") > 0) { //если верный формат
						if (options.split(":")[0].equals(option)) { //если нужная - добавляем значение
							if (ad == null) {
								double old = 0;
								try {
									old = Double.parseDouble(options.split(":")[1]);
								} catch (Exception e) {e.printStackTrace();}
							options = option + ":" + String.valueOf(old+add);
							} else options = option + ":" + ad;
							waits.set(i, (waits.get(i).substring(0, waits.get(i).lastIndexOf("/")) + "/" + options));
						} else { //если нет - добавляем опцию
							if (ad == null) options += ";" + option + ":1"; else options += ";" + option + ":" + ad;
							waits.set(i, (waits.get(i).substring(0, waits.get(i).lastIndexOf("/")) + "/" + options));
						}
					} else { //если не верный формат
						if (ad == null) options = option + ":1"; else options = option + ":" + ad;
						waits.set(i, (waits.get(i).substring(0, waits.get(i).lastIndexOf("/")) + "/" + options));
					}
				}
				break;
			} else if (waits.get(i).split("/")[0].split(",")[1].equals(name)) { //если нет части с данными
				waits.set(i, waits.get(i) + "/" + option + ":" + String.valueOf(add)); //добавляем ее
				break;
			}
		}
	}
	
	private String getOpt(String str, String opt) { //получение опции из строки БД
		String result = null;
		if (str.split("/").length == 3) { //если есть секция с данными
			String option = str.split("/")[2];
			if (option.indexOf(opt) != -1) { //если в ней есть искомая опция
				if (option.indexOf(";") != -1) { //если опций в секции несколько
					String options[] = option.split(";");
					for (int i = 0; i < options.length; i++) { //ищем нужную
						if (options[i].split(":")[0].equals(opt)) { //если она найдена
							option = options[i].split(":")[1];
							result = option;
							break;
						}
					}
				} else if (option.indexOf(":") != -1) { //есди опция в секции одна и нужная
					if (option.split(":")[0].equals(opt)) {
						option = option.split(":")[1];
						result = option;
					}
				}
			}
		}
		return result;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userTeleport(PlayerTeleportEvent event) { //обработка телепортаций
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chteleport && !waits.isEmpty() & !event.isCancelled()) {
			optPlus(event.getPlayer().getName(), "cctp", 1, null);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userMove(PlayerMoveEvent event) { //обработка перемещения
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chmove && !waits.isEmpty() & !event.isCancelled()) {
			optPlus(event.getPlayer().getName(), "mdist", ((Math.ceil(event.getFrom().distance(event.getTo()) * 100) / 100)), null);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userCommand(PlayerCommandPreprocessEvent event) { //обработка использования команд
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chcommand && !waits.isEmpty() & !event.isCancelled()) {
			String name = event.getPlayer().getName();
			optPlus(name, "ccom", 1, null); //обработка опции с кол-вом использованых команд
			if (main.conf.obcom.length > 0) {
				String message = event.getMessage();
				for (int i = 0; i < main.conf.obcom.length; i++) { //поиск совпадений с обязательными командами
					if (message.indexOf(main.conf.obcom[i]) != -1) { //если совпадение найдено
						boolean ok = true;
						if (!waits.isEmpty()) {
							for (int n = 0; n < waits.size(); n++) {
								if (waits.get(n).split("/")[0].split(",")[1].equals(name)) {
									if (waits.get(n).split("/").length == 3) {
										String ch = getOpt(waits.get(n), "obcom");
										if (ch != null) {
											if (main.conf.obcom[i].indexOf(ch) != -1) ok = false; //исключение повторных команд
											break;
										}
									}
								}
							}
						}
						if (ok) {
							optPlus(name, "obcom", 1, main.conf.obcom[i]);
							break;
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userChat(AsyncPlayerChatEvent event) { //обработка сообщений в чат
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chchat && !waits.isEmpty() & !event.isCancelled()) {
			optPlus(event.getPlayer().getName(), "cchat", 1, null);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userInteract(PlayerInteractEvent event) { //обработка взаимодействий с блоками
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chinteract && !waits.isEmpty() & !event.isCancelled()) {
			optPlus(event.getPlayer().getName(), "iinter", 1, null);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void userClick(InventoryClickEvent event) { //обработка кликов в инвентаре
		if (main.conf.ref & main.conf.actwait & main.conf.chact & main.conf.chclick && !waits.isEmpty() & !event.isCancelled()) {
			optPlus(event.getWhoClicked().getName(), "cclick", 1, null);
		}
	}
	
	private class rwait extends TimerTask { //класс обработки БД по таймеру
		@Override
		public void run() { //метод, вызываемый таймером
			if (main.conf.actwait) { //если задержка перед выдачей приза включена
				if (!waits.isEmpty()) { //если БД не пуста
					for (int i = 0; i < waits.size(); i++) {
						Date date = new Date();
						long stamp = date.getTime();
						long oldstamp = 0;
						try {
							oldstamp = Long.parseLong(waits.get(i).split("/")[1].trim());
						} catch (Exception e) {e.printStackTrace();}
						long min = (stamp / 1000) - (oldstamp / 1000);
						if (min >= main.conf.waitmin * 60) { //если время ожидания закончилось
							boolean get = true;
							if (main.conf.chact) { //если включена проверка активности
								if (main.conf.chmove) { //проверка пройденной дистанции
									String opt = getOpt(waits.get(i), "mdist");
									if (opt != null) {
										double dist = 0;
										try {
											dist = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (dist < main.conf.mdist) get = false;
									} else get = false;
								}
								if (main.conf.chcommand) { //проверка введенных комманд
									String opt = getOpt(waits.get(i), "ccom");
									if (opt != null) { //проверка количества
										double com = 0;
										try {
											com = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (com < main.conf.ccom) get = false;
										if (get && main.conf.obcom.length > 0) { //проверка наличия всех обязательных команд
											String optc = getOpt(waits.get(i), "obcom");
											if (optc != null) {
												double ch = 1;
												if (optc.indexOf(",") != -1) {
													ch = optc.split(",").length;
												}
												if (ch != main.conf.obcom.length) get = false;
											} else get = false;
										}
									} else get = false;
								}
								if (main.conf.chchat) { //проверка сообщений в чат
									String opt = getOpt(waits.get(i), "cchat");
									if (opt != null) {
										double chat = 0;
										try {
											chat = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (chat < main.conf.cchat) get = false;
									} else get = false;
								}
								if (main.conf.chinteract) { //проверка взаимодействий с блоками
									String opt = getOpt(waits.get(i), "iinnter");
									if (opt != null) {
										double it = 0;
										try {
											it = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (it < main.conf.iinter) get = false;
									} else get = false;
								}
								if (main.conf.chclick) { //проверка кликов в инвентаре
									String opt = getOpt(waits.get(i), "cclick");
									if (opt != null) {
										double click = 0;
										try {
											click = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (click < main.conf.cclick) get = false;
									} else get = false;
								}
								if (main.conf.chteleport) { //проверка телепортаций
									String opt = getOpt(waits.get(i), "cctp");
									if (opt != null) {
										double tp = 0;
										try {
											tp = Double.parseDouble(opt);
										} catch (Exception e) {e.printStackTrace();}
										if (tp < main.conf.cctp) get = false;
									} else get = false;
								}
							}
							String referal = waits.get(i).split("/")[0].split(",")[1];
							String refer = waits.get(i).split("/")[0].split(",")[0];
							if (get) { //если проверки пройдены успенно
								if (ut.isOnline(referal) && ut.isOnline(refer)) { //если оба игрока онлайн
									//выдача приза и отправка сообщений
									for (int n = 0; n < main.conf.refcom.length; n++) {
										try {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.refcom[n].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
										} catch (Exception e) {};
									}
									Player refplayer = Bukkit.getPlayer(referal);
									Player referplayer = Bukkit.getPlayer(refer);
									if (main.conf.refmess) main.log.info(refer + " успешно заинтересовал " + referal);
									if (main.conf.okwtorefer.length() > 1) refplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.okwtorefer.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())));
									if (main.conf.okwtoreferer.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.okwtoreferer.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())));
									if (main.conf.okwait.length() > 1) Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', main.conf.okwait.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())), "vomine.ref.notify");
								} else { //если кто-то вышел
									if (main.conf.refmess) main.log.info(refer + " успешно заинтересовал " + referal + ", но кто-то из них вышел из игры");
									if (ut.isOnline(refer)) { //если пригласивший онлайн - выдача утешительного приза
										Player referplayer = Bukkit.getPlayer(refer);
										if (main.conf.nowait.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.nowait.replaceAll("%refer%", refer).replaceAll("%referer%", referplayer.getName())));
									}
								}
								setBest(referal, refer); //запись статистики
								if (main.conf.stat) { //обновление статистики
									File st = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
									int best = 1;
									if (st.isFile()) {
										configLoader loader = ut.cfload.getLoader();
										loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
										ut.offLog();
										if (loader.isSet("best")) {
											best += loader.getInt("best");
										}
										ut.onLog();
									} else {
										File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat");
										if (!fold.isDirectory()) fold.mkdirs();
									}
									configWriter writer = ut.cfwrite.getWriter();
									writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
									writer.setOption("best", String.valueOf(best));
								}
								File db = new File(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
								if (db.isFile()) { //если БД есть
									ut.fdb.read(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
									if (!ut.fdb.get().isEmpty()) { //если не пуста
										ArrayList<String> fdb = ut.fdb.get();
										for (int j = 0; j < fdb.size(); j++) { //поиск
											/*
											 * 0 - ник приглашенного
											 * 1 - ип
											 * 2 - хост
											 * 3 - юид
											 * 4 - ник пригласившего
											 * 5 - ип
											 * 6 - хост
											 * 7 - юид
											 * 8 - дата
											 * 9 - заинтересован ли
											 */
											String mdb[] = fdb.get(j).split(";");
											if (mdb[0].equalsIgnoreCase(referal)) { //обновление информации в БД
												fdb.set(j, (fdb.get(j).substring(0, fdb.get(j).lastIndexOf(";")) + ";true"));
												ut.fdb.write(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
												break;
											}
										}
										ut.fdb.get().clear();
									}
								}
							} else { //если нет (игрок не заинтересовался)
								if (main.conf.utpriz && !main.conf.utnowait) { //если включена выдача утешительного приза
									if (ut.isOnline(referal) && ut.isOnline(refer)) { //если оба игрока онлайн
										for (int n = 0; n < main.conf.utcom.length; n++) {
											try {
												Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.utcom[n].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
											} catch (Exception e) {};
										}
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal + ", выдан утешительный приз");
										Player refplayer = Bukkit.getPlayer(referal);
										Player referplayer = Bukkit.getPlayer(refer);
										if (main.conf.utmess.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.utmess.replaceAll("%refer%", refplayer.getName().replaceAll("%referer%", referplayer.getName()))));
									} else { //если кто-то вышел
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal + ", и кто-то из них вышел из игры (утешительный приз не выдан)");
										if (ut.isOnline(refer)) { //если пригласивший онлайн
											for (int n = 0; n < main.conf.utcom.length; n++) {
												try {
													Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.utcom[n].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
												} catch (Exception e) {};
											}
											Player referplayer = Bukkit.getPlayer(refer);
											if (main.conf.utmess.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.utmess.replaceAll("%refer%", refer).replaceAll("%referer%", referplayer.getName())));
										}
									}
								} else if (!main.conf.utpriz) { //если выдача утешительного приза выключена
									if (ut.isOnline(refer)) { //если пригласивший онлайн
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal);
										Player referplayer = Bukkit.getPlayer(refer);
										if (main.conf.nowait.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.nowait.replaceAll("%refer%", referal.replaceAll("%referer%", refer))));
									} else { //если вышел
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal + ", и сам вышел");
									}
								} else {
									if (ut.isOnline(refer)) { //если пригласивший онлайн
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal);
										Player referplayer = Bukkit.getPlayer(refer);
										if (main.conf.nowait.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.nowait.replaceAll("%refer%", referal.replaceAll("%referer%", refer))));
									} else { //если вышел
										if (main.conf.refmess) main.log.info(refer + " не заинтересовал " + referal + ", и сам вышел");
									}
								}
							}
							if (ut.isOnline(refer)) checkAch(refer); //проверка на ачивки
							if (main.conf.refdebug) main.log.info("Обработано: " + waits.get(i));
							waits.remove(i); //удаление отработанной строки
						}
					}
				}
			}
		}
	}
	
	private Timer timer = null;
	private void onTimer() { //включение таймера для обработки БД
		Timer tim = new Timer();
		timer = tim;
		tim.schedule(new rwait(), 2000, 2000);
	}
	public void offTimer() { //выключение
		if (timer != null) {
			timer.cancel();
		}
	}
	
	private void setBest(String referal, String refer) { //запись статистики о заинтересованном реферале
		File rf = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
		if (rf.isFile()) { //если файл на пригласившего уже есть
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
			ArrayList<String> rfdata = new ArrayList<String>();
			ut.offLog();
			if (loader.isSet("bestrefers")) { //проверяем, есть ли в нем нужное значение
				String bref[] = loader.getStringArray("bestrefers");
				for (int i = 0; i < bref.length; i++) rfdata.add(bref[i]); //добавляем его
			}
			ut.onLog();
			rfdata.add(referal); //добавляем ник реферала в список
			String rfd[] = new String[rfdata.size()];
			for (int i = 0; i < rfdata.size(); i++) rfd[i] = rfdata.get(i);
			configWriter writer = ut.cfwrite.getWriter();
			writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
			writer.setArray("bestrefers", rfd, false); //перезаписываем
		} else if (main.conf.stat) { //если файла нет и статистика включена
			configWriter writer = ut.cfwrite.getWriter();
			writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
			String rfd[] = {referal};
			writer.setArray("bestrefers", rfd, false); //записываем значение
		}
		if (main.conf.stat) {
			configWriter writer = ut.cfwrite.getWriter();
			writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + referal.toLowerCase() + ".yml");
			writer.setOption("best", "true"); //запись значения в файл приглашенного, что он заинтересованный
		}
		File all = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
		int best = 1;
		if (all.isFile()) { //обновление общей статистики
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
			ut.offLog();
			if (loader.isSet("best")) {
				best += loader.getInt("best");
			}
			ut.onLog();
		} else if (main.conf.stat) { //создание файла общей статистики, если его нет
			configWriter writer = ut.cfwrite.getWriter();
			writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
			writer.setOption("best", String.valueOf(best));
		}
	}
	
	private void checkAch(String refer) { //проверка на ачивки
		if (main.conf.refachivment) { //если опция включена
			File ch = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
			if (ch.isFile()) { //если есть файл реферала
				int ref = 0, bref = 0, nar = 0;
				configLoader loader = ut.cfload.getLoader();
				loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
				ut.offLog();
				if (loader.isSet("refers")) ref += loader.getStringArray("refers").length; //чтение значений для сверки
				if (loader.isSet("bestrefers")) bref += loader.getStringArray("bestrefers").length;
				if (loader.isSet("ipnar")) nar += loader.getInt("ipnar");
				if (loader.isSet("uuidnar")) nar += loader.getInt("uuidnar");
				ArrayList<String> aach = new ArrayList<String>();
				if (loader.isSet("ach")) { //заполнение списка старых ачивок
					String achh[] = loader.getStringArray("ach");
					for (int n = 0; n < achh.length; n++) aach.add(achh[n]);
				}
				ut.onLog();
				for (int i = 0; i < main.conf.refachset.length; i++) { //поиск по массиву ачивок
					/*
					 * 0 - рефералы
					 * 1 - лучшие рефералы
					 * 2 - нарушения
					 * 3 - ачивка (отображение в списке)
					 * 4 - уведомление в чат
					 * 5 - уведомление игроку
					 * >= 6 - команды
					 */
					String ach = main.conf.refachset[i];
					int rref = -1, bbref = -1, nnar = -1;
					try { //считывание параметров из элемента массива
						if (ach.split(";")[0].trim().indexOf("-") == -1) rref = Integer.parseInt(ach.split(";")[0].trim());
						if (ach.split(";")[1].trim().indexOf("-") == -1) bbref = Integer.parseInt(ach.split(";")[1].trim());
						if (ach.split(";")[2].trim().indexOf("-") == -1) nnar = Integer.parseInt(ach.split(";")[2].trim());
					} catch (Exception e) {e.printStackTrace();}
					if (rref == ref | rref == -1 && bbref == bref | bbref == -1 && nar == nnar | nnar == -1 && rref+bbref+nnar != -3) { //если ачивка нашлась - обрабатываем
						boolean ok = true;
						String mach[] = ach.split(";");
						String add = mach[3].trim().replaceAll("%refer%", refer).replaceAll("%refnum%", String.valueOf(ref)).replaceAll("%bestnum%", String.valueOf(bref)).replaceAll("%narnum%", String.valueOf(nar));
						for (int n = 0; n < main.conf.refachset.length; n++) {
							for (int y = 0; y < aach.size(); y++) {
								if (add.equals(aach.get(y))) {
									ok = false;
									break;
								}
							}
							if (!ok) break;
						}
						if (ok) { //предохранение от выдачи одинаковых ачивок
							for (int n = 6; n < mach.length; n++) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), mach[n].trim().replaceAll("%refer%", refer));
							}
							if (main.conf.rachbroadcast) Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', mach[4].trim().replaceAll("%refer%", refer).replaceAll("%refnum%", String.valueOf(ref)).replaceAll("%bestnum%", String.valueOf(bref)).replaceAll("%narnum%", String.valueOf(nar))), "vomine.ref.notify");
							if (main.conf.rachtoplay) Bukkit.getPlayer(refer).sendMessage(ChatColor.translateAlternateColorCodes('&', mach[5].trim().replaceAll("%refer%", refer).replaceAll("%refnum%", String.valueOf(ref)).replaceAll("%bestnum%", String.valueOf(bref)).replaceAll("%narnum%", String.valueOf(nar))));
							if (main.conf.refmess) main.log.info(ChatColor.translateAlternateColorCodes('&', mach[4].trim().replaceAll("%refer%", refer).replaceAll("%refnum%", String.valueOf(ref)).replaceAll("%bestnum%", String.valueOf(bref)).replaceAll("%narnum%", String.valueOf(nar))));
							aach.add(add);
							String achh[] = new String[aach.size()];
							for (int n = 0; n < aach.size(); n++) achh[n] = aach.get(n);
							configWriter writer = ut.cfwrite.getWriter();
							writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
							writer.setArray("ach", achh, false);
						}
					}
				}
			}
		}
	}
	
	void onRef(String referal, String refer) { //обработка ввода команды на приглашение
		ref r = new ref(referal, refer);
		r.start();
	}
	
	String[] checkUser(String user) { //получение информации о игроке, если на него есть файл
		String result[] = null;
		File ch = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
		if (ch.isFile()) {
			ArrayList<String> data = new ArrayList<String>();
			data.add("---> Информация о " + user + ":");
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
			ut.offLog();
			/*
			 * blocked - заблокирован ли
			 * ipnar - количество попыток стать рефералом самого себя (одинаковая подсеть)
			 * uuidnar - тоже самое, только одинаковые uuid
			 * refers - ники приглашенных
			 * refer - кто пригласил
			 * bestrefers - ники заинтересованных приглашенных
			 * best - заинтересованный ли приглашенный
			 * ach - достижения
			 */
			if (loader.isSet("blocked")) data.add("Заблокирован: " + loader.getString("blocked"));
			if (loader.isSet("refer")) data.add("Пришлашен: " + loader.getString("refer"));
			if (loader.isSet("best")) data.add("Заинтересованный: " + loader.getString("best").replaceAll("true", "да").replaceAll("false", "нет"));
			if (loader.isSet("ipnar")) data.add("Использование твинков с тем же ip: " +loader.getString("ipnar"));
			if (loader.isSet("uuidnar")) data.add("Использование твинков с тем же uuid: " + loader.getString("uuidnar"));
			if (loader.isSet("refers")) {
				String refers[] = loader.getStringArray("refers");
				String refrs = "-> " + refers[0];
				data.add("Приведенные рефералы (" + String.valueOf(refers.length) + "):");
				for (int i = 1; i < refers.length; i++) refrs += ", " + refers[i];
				data.add(refrs);
			}
			if (loader.isSet("bestrefers")) {
				String bestrefers[] = loader.getStringArray("bestrefers");
				String brefrs = "-> " + bestrefers[0];
				data.add("Заинтересованные рефералы (" + String.valueOf(bestrefers.length) + "):");
				for (int i = 1; i < bestrefers.length; i++) brefrs += ", " + bestrefers[i];
				data.add(brefrs);
			}
			if (loader.isSet("ach")) {
				String ach[] = loader.getStringArray("ach");
				String aach = "-> " + ach[0];
				data.add("Достижения (" + String.valueOf(ach.length) + "):");
				for (int i = 1; i < ach.length; i++) aach += ", " + ach[i];
				data.add(ChatColor.translateAlternateColorCodes('&', aach));
			}
			ut.onLog();
			result = new String[data.size()];
			for (int i = 0; i < data.size(); i++) result[i] = data.get(i);
		}
		return result;
	}
	
	String[] getStat() { //получение общей статистики, если есть файл
		String result[] = null;
		File ch = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
		if (ch.isFile()) {
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
			ArrayList<String> data = new ArrayList<String>();
			ut.offLog();
			/*
			 * ref - кол-во пригласивших игроков
			 * referred - кол-во приглашенных
			 * noref - попыток жульничества
			 * best - заинтересованных
			 */
			if (loader.isSet("ref")) data.add("Пригласивших: " + loader.getString("ref"));
			if (loader.isSet("referred")) data.add("Приглашенных: " + loader.getString("referred"));
			if (loader.isSet("best")) data.add("Заинтересованных: " + loader.getString("best"));
			if (loader.isSet("noref")) data.add("Попыток жульничества: " + loader.getString("noref"));
			ut.onLog();
			result = new String[data.size()];
			for (int i = 0; i < data.size(); i++) result[i] = data.get(i);
		}
		return result;
	}
	
	String[] checkUserInDB(String user) { //получение информации о игроке из БД
		String result[] = null;
		File db = new File(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
		if (db.isFile()) { //если БД есть
			ut.fdb.read(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
			if (!ut.fdb.get().isEmpty()) { //если не пуста
				ArrayList<String> fdb = ut.fdb.get();
				for (int i = 0; i < fdb.size(); i++) { //поиск
					/*
					 * 0 - ник приглашенного
					 * 1 - ип
					 * 2 - хост
					 * 3 - юид
					 * 4 - ник пригласившего
					 * 5 - ип
					 * 6 - хост
					 * 7 - юид
					 * 8 - дата
					 * 9 - заинтересован ли
					 */
					String mdb[] = fdb.get(i).split(";");
					if (mdb[0].equalsIgnoreCase(user)) { //если игрок найден
						result = new String[12];
						result[0] = "---> Информация о " + user;
						result[1] = "Ник: " + mdb[0];
						result[2] = "IP адрес: " + mdb[1];
						result[3] = "Хост: " + mdb[2];
						result[4] = "UUID: " + mdb[3];
						result[5] = "Ник пригласившего: " + mdb[4];
						result[6] = "IP пригласившего: " + mdb[5];
						result[7] = "Хост пригласившего: " + mdb[6];
						result[8] = "UUID пригласившего: " + mdb[7];
						try {
							long d = Long.parseLong(mdb[8]);
							Date date = new Date();
							long stamp = date.getTime();
							long min = (stamp / 1000) - (d / 1000);
							mdb[8] = String.valueOf((Math.ceil((double) min / 60 / 60 * 100) / 100));
						} catch (Exception e) {e.printStackTrace();}
						result[9] = "Дата: " + mdb[8] + " часов назад";
						result[10] = "Заинтересован: " + mdb[9].replaceAll("true", "да").replaceAll("false", "нет");
						result[11] = "<--->";
						break;
					}
				}
			}
			ut.fdb.get().clear(); //очистка БД из памяти
		}
		return result;
	}
	
	boolean blockUser(String user) { //блокировка игрока
		boolean result = false;
		File u = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
		if (u.isFile()) {
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
			ut.offLog();
			if (loader.isSet("blocked")) { //есди опция уже есть
				if (!loader.getBoolean("blocked")) { //проверка значения
					configWriter writer = ut.cfwrite.getWriter();
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
					writer.setOption("blocked", "true");
					result = true;
				}
			} else { //если нету - добавляем
				configWriter writer = ut.cfwrite.getWriter();
				writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
				writer.setOption("blocked", "true");
				result = true;
			}
			ut.onLog();
		} else {
			File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
			if (!fold.isDirectory()) fold.mkdirs();
			configWriter writer = ut.cfwrite.getWriter();
			writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
			writer.setOption("blocked", "true");
			result = true;
		}
		return result;
	}
	
	boolean unlockUser(String user) { //разблокировка игрока
		boolean result = false;
		File u = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
		if (u.isFile()) {
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
			ut.offLog();
			if (loader.isSet("blocked")) {
				if (loader.getBoolean("blocked")) {
					configWriter writer = ut.cfwrite.getWriter();
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
					writer.setOption("blocked", "false");
					result = true;
				}
			}
			ut.onLog();
		}
		return result;
	}
	
	String[] getRefers() { //получение списка пригласивших кого-либо
		String result[] = null;
		File r = new File(ut.getPatch() + "/plugins/Vomine/ref/refers.yml");
		if (r.isFile()) {
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/refers.yml");
			ut.offLog();
			if (loader.isSet("refers")) {
				result = loader.getStringArray("refers");
			}
			ut.onLog();
		}
		return result;
	}
	
	boolean clearNar(String user) { //очистка нарушений игрока
		boolean result = false;
		File u = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
		if (u.isFile()) {
			configLoader loader = ut.cfload.getLoader();
			loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
			ut.offLog();
			if (loader.isSet("ipnar")) { //одинаковая подсеть
				configWriter writer = ut.cfwrite.getWriter();
				writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
				writer.setOption("ipnar", "0");
				result = true;
			}
			if (loader.isSet("uuidnar")) { //одинаковые UUID-ы
				configWriter writer = ut.cfwrite.getWriter();
				writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + user.toLowerCase() + ".yml");
				writer.setOption("uuidnar", "0");
				result = true;
			}
			ut.onLog();
		}
		return result;
	}
	
	private class ref extends Thread { //класс обработки команды на приглашение
	String referal, refer;
	ref(String rr, String rf) {
		referal = rr;
		refer = rf;
	}
		
	@Override
	public void run() {
		if (main.conf.ref) {
			Player refplayer = Bukkit.getPlayer(referal);
			Player referplayer = null;
			if (ut.isOnline(refer)) referplayer = Bukkit.getPlayer(refer); else if (main.conf.offline) { //попытка чтения информации, если пригласивший в оффлайне
				String world = null;
				try {
					BufferedReader reader = new BufferedReader(new FileReader(ut.getPatch() + "/server.properties"));
					String str = null;
					int l = 0;
					do {
						str = reader.readLine();
						if (str != null) {
							l++;
							if (str.split("=")[0].equals("level-name")) {
								world = str.split("=")[1];
								break;
							}
						}
					} while (str != null && l < 2147483647);
					reader.close();
				} catch (Exception e) {e.printStackTrace();}
				String list[] = new File(ut.getPatch() + "/" + world + "/playerdata").list();
				for (int i = 0; i < list.length; i++) {
					Player plr = Bukkit.getOfflinePlayer(UUID.fromString(list[i].substring(0, list[i].lastIndexOf(".")))).getPlayer();
					if (plr != null) {
						if (plr.getName().equals(refer)) {
							referplayer = plr;
							break;
						}
					}
				}
			}
			boolean get = true; boolean found = false;
			if (referplayer != null) found = true;
			if (referplayer != null) {
				if (refplayer.equals(referplayer)) get = false;
			}
			boolean blocked = false; //заблокирована ли возможность приглашать игроков
			boolean neew = true; //в первый ли раз происходит приглашение игрока
			File ch = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
			if (ch.isFile()) { //обработка
				configLoader loader = ut.cfload.getLoader();
				loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
				ut.offLog();
				if (loader.isSet("blocked")) {
					if (loader.getBoolean("blocked")) {get = false; blocked = true;}
				}
				if (loader.isSet("refers")) neew = false;
				ut.onLog();
			}
			if (main.conf.ipcheck && get) { //сверка подсетей
				String refip = refplayer.getAddress().getAddress().toString();
				refip = refip.substring((refip.indexOf("/")+1), refip.length());
				String referip = null;
				if (referplayer != null) {
						referip = referplayer.getAddress().getAddress().toString();
						referip = referip.substring((referip.indexOf("/")+1), referip.length());
				}
				if (referip == null) get = false; else {
					if (refip.substring(0, refip.lastIndexOf(".")).equals(referip.substring(0, referip.lastIndexOf(".")))) get = false;
				}
				if (main.conf.blacklist) { //если включена такая опция, запись в файл игрока о нарушении
					if (!get && referip != null) {
						File d = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
						if (!d.isDirectory()) d.mkdirs();
						File f = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						int ipnar = 1;
						if (f.isFile()) {
							configLoader loader = ut.cfload.getLoader();
							loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
							ut.offLog();
							if (loader.isSet("ipnar")) ipnar += loader.getInt("ipnar");
							ut.onLog();
						} else {
							File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
							if (!fold.isDirectory()) fold.mkdirs();
						}
						configWriter writer = ut.cfwrite.getWriter();
						writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						writer.setOption("ipnar", String.valueOf(ipnar));
					}
				}
			}
			if (main.conf.hostcheck && get) { //сверка хоста
				String refhost = refplayer.getAddress().getHostName();
				String referhost = null; if (referplayer != null) referhost = referplayer.getAddress().getHostName();
				if (referhost != null && refhost != null) {
					if (refhost.equals(referhost)) get = false;
				}
			}
			if (main.conf.datacheck && get) { //сверка даты (приглашенный игрок меньше на сервере, чем пригласивший)
				long refdate = refplayer.getFirstPlayed();
				long referdate = -1; if (referplayer != null) referdate = referplayer.getFirstPlayed();
				if (referdate != -1) {
					if (refdate < referdate) get = false;
				} else get = false;
			}
			if (main.conf.newcheck) { //проверка, что игрок новый
				long refdate = refplayer.getFirstPlayed();
				Date date = new Date();
				long time = date.getTime();
				long chtime = (time / 1000) - (refdate / 1000);
				if (chtime > main.conf.newmin * 60) get = false;
			}
			if (main.conf.uuidcheck && get) { //сверка uuid-ов
				if (referplayer != null) {
					if (refplayer.getUniqueId().equals(referplayer.getUniqueId())) get = false;
				} else get = false;
				if (main.conf.blacklist) { //запись о нарушении, если опция включена
					if (!get && referplayer != null) {
						File f = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						int uuidnar = 1;
						if (f.isFile()) {
							configLoader loader = ut.cfload.getLoader();
							loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
							ut.offLog();
							if (loader.isSet("uuidnar")) uuidnar += loader.getInt("uuidnar");
							ut.onLog();
						} else {
							File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
							if (!fold.isDirectory()) fold.mkdirs();
						}
						configWriter writer = ut.cfwrite.getWriter();
						writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						writer.setOption("uuidnar", String.valueOf(uuidnar));
					}
				}
			}
			if (get) { //поиск ника и подсети по БД (защита от мультиакков)
				File d = new File(ut.getPatch() + "/plugins/Vomine/ref");
				if (!d.isDirectory()) {if (!d.mkdirs()) main.log.warn("не удалось создать папку " + d.getAbsolutePath());}
				File f = new File(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
				if (f.isFile()) ut.fdb.read(ut.getPatch() + "/plugins/Vomine/ref/ref.db"); //загрузка БД в пямять
				if (!ut.fdb.get().isEmpty()) {
				for (int i = 0; i < ut.fdb.get().size(); i++) {
					String refip = refplayer.getAddress().getAddress().toString();
					refip = refip.substring((refip.indexOf("/")+1), refip.length());
					refip = refip.substring(0, refip.lastIndexOf("."));
					String refname = refplayer.getName();
					String refhost = refplayer.getAddress().getAddress().getCanonicalHostName();
					String refuuid = refplayer.getUniqueId().toString();
					String str = ut.fdb.get().get(i);
					String oldip = str.split(";")[1];
					oldip = oldip.substring(0, oldip.lastIndexOf("."));
					String oldname = str.split(";")[0];
					String oldhost = str.split(";")[2];
					String olduuid = str.split(";")[3];
					if (refname.equalsIgnoreCase(oldname)) {
						get = false;
						break;
					}
					if (main.conf.ipcheck) { //сверка подсети
						if (refip.equals(oldip)) {
							get = false;
							break;
						}
					}
					if (main.conf.hostcheck & main.conf.bhostcheck && refhost != null) { //сверка хоста
						if (refhost.equals(oldhost)) {
							get = false;
							break;
						}
					}
					if (main.conf.uuidcheck) { //сверка uuid-а
						if (refuuid.equals(olduuid)) {
							get = false;
							break;
						}
					}
				}
				}
			}
			if (get) { //если приглашение удалось
				Date date = new Date();
				long stamp = date.getTime();
				//вывод сообщений
				if (main.conf.refmess) main.log.info(refer + " успешно пригласил " + referal);
				if (main.conf.torefer.length() > 1) refplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.torefer.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())));
				if (main.conf.toreferer.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.toreferer.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())));
				if (main.conf.toall.length() > 1) Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', main.conf.toall.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName())), "vomine.ref.notify");
				if (!main.conf.actwait) { //если проверка активности не включена
					for (int i = 0; i < main.conf.refcom.length; i++) {
						try {
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.refcom[i].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
						} catch (Exception e) {};
					}
				} else { //есди включена
					if (waits.size() < main.conf.wlimit) { //если лимит не превышен - заносим информацию в БД
						/*
						 * 0 - ник
						 * 1 - таймштамп
						 * 2 - данные (опция:значение;опция...)
						 * реферал,рефер/таймштамп/данные
						 */
						waits.add(refer + "," + referal + "/" + String.valueOf(stamp));
					} else { //если превышен
						if (main.conf.refmess) main.log.info("достигнут лимит базы данных, " + refer + " не дождется основного приза");
						if (!main.conf.utnowait && main.conf.utpriz) { //выдача утешительного приза, если его выдача без ожидания проверки выключена
							for (int n = 0; n < main.conf.utcom.length; n++) {
								try {
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.utcom[n].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
								} catch (Exception e) {};
							}
						}
						if (main.conf.erwait.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.erwait));
					}
					//вывод сообщений
					if (main.conf.stwait.length() > 1) referplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.stwait.replaceAll("%refer%", refplayer.getName()).replaceAll("%referer%", referplayer.getName()).replaceAll("%min%", String.valueOf(main.conf.waitmin))));
					if (main.conf.utnowait && main.conf.utpriz) { //если включена выдача утешительного приза сразу
						for (int n = 0; n < main.conf.utcom.length; n++) {
							try {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), main.conf.utcom[n].replaceAll("%refer%", refer).replaceAll("%ref%", referal));
							} catch (Exception e) {};
						}
						if (main.conf.refmess) main.log.info(refer + " возможно заинтересует " + referal + ", утешительный приз выдан сразу");
					}
				}
				/*
				 * 0 - ник приглашенного
				 * 1 - ип
				 * 2 - хост
				 * 3 - юид
				 * 4 - ник пригласившего
				 * 5 - ип
				 * 6 - хост
				 * 7 - юид
				 * 8 - дата
				 * 9 - заинтересован ли
				 * 
				 * БД уже прогружена при проверке (а если get = false, этот блок не выполняется)
				 */
				String refip = refplayer.getAddress().getAddress().toString();
				refip = refip.substring((refip.indexOf("/")+1), refip.length());
				String referip = referplayer.getAddress().getAddress().toString();
				referip = referip.substring((referip.indexOf("/")+1), referip.length());
				String refhost = refplayer.getAddress().getAddress().getCanonicalHostName();
				if (refhost == null) refhost = "null"; else if (refhost.length() == 0) refhost = "empty";
				String referhost = null; if (referplayer != null) referhost = referplayer.getAddress().getAddress().getCanonicalHostName();
				if (referhost == null) referhost = "null"; else if (referhost.length() == 0) referhost = "empty";
				String dbadd = refplayer.getName() + ";" //обновление файловой БД с приглашенными
				+ refip + ";"
						+ refhost + ";"
				+ refplayer.getUniqueId().toString() + ";"
						+ referplayer.getName() + ";"
				+ referip + ";"
						+ referhost + ";"
				+ referplayer.getUniqueId().toString() + ";"
						+ String.valueOf(stamp) + ";"
						+ "false";
				ut.fdb.get().add(dbadd);
				ut.fdb.write(ut.getPatch() + "/plugins/Vomine/ref/ref.db");
				if (main.conf.refdebug) main.log.info("Записано в БД: " + dbadd);
				if (main.conf.stat) { //если включен сбор статистики (обновление общей статистики)
					File d = new File(ut.getPatch() + "/plugins/Vomine/ref/stat");
					if (!d.isDirectory()) d.mkdirs();
					File st = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
					int ref = 1; if (!neew) ref = 0;
					int referred = 1;
					if (st.isFile()) {
						configLoader loader = ut.cfload.getLoader();
						loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
						ut.offLog();
						if (loader.isSet("ref")) ref += loader.getInt("ref");
						if (loader.isSet("referred")) referred += loader.getInt("referred");
						ut.onLog();
					} else {
						File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat");
						if (!fold.isDirectory()) fold.mkdirs();
					}
					configWriter writer = ut.cfwrite.getWriter();
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
					writer.setOption("ref", String.valueOf(ref));
					writer.setOption("referred", String.valueOf(referred));
					File sp = new File(ut.getPatch() + "/plugins/Vomine/ref/refers.yml"); //обновление списка приглашающих (для удобства админа)
					ArrayList<String> refers = new ArrayList<String>();
					if (sp.isFile()) { //если есть файл - обновляем его
						configLoader loader = ut.cfload.getLoader();
						loader.load(ut.getPatch() + "/plugins/Vomine/ref/refers.yml");
						boolean add = true;
						ut.offLog();
						if (loader.isSet("refers")) {
							String reff[] = loader.getStringArray("refers");
							for (int i = 0; i < reff.length; i++) { //обработка старого массива
								if (!reff[i].equalsIgnoreCase(refer)) {
									refers.add(reff[i]);
								} else add = false;
							}
						}
						ut.onLog();
						if (add) refers.add(refer); //исключение повторных добавлений одного ника
						String reff[] = new String[refers.size()];
						for (int i = 0; i < refers.size(); i++) reff[i] = refers.get(i);
						writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/refers.yml");
						writer.setArray("refers", reff, false);
					} else {
						String reff[] = {refer};
						writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/refers.yml");
						writer.setArray("refers", reff, false);
					}
				}
				if (main.conf.stat | main.conf.blacklist) { //если включен сбор статистики или запись о нарушениях (запись в файлы игроков)
					File d = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
					if (!d.isDirectory()) d.mkdirs();
					configWriter writer = ut.cfwrite.getWriter();
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + referal.toLowerCase() + ".yml");
					writer.setOption("refer", refer);
					File fr = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
					ArrayList<String> refki = new ArrayList<String>();
					if (fr.isFile()) {
						configLoader loader = ut.cfload.getLoader();
						loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						ut.offLog();
						if (loader.isSet("refers")) {
							String refk[] = loader.getStringArray("refers");
							for (int i = 0; i < refk.length; i++) refki.add(refk[i]);
						}
						ut.onLog();
					} else {
						File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
						if (!fold.isDirectory()) fold.mkdirs();
					}
					refki.add(referal);
					String refk[] = new String[refki.size()];
					for (int i = 0; i < refki.size(); i++) refk[i] = refki.get(i);
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
					writer.setArray("refers", refk, false);
				}
				checkAch(refer); //проверка на достижения
			} else { //если проверка на мошенничество не пройдена
				//вывод сообщений
				if (found && !blocked) refplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.noref));
				else if (!found && !blocked) refplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.notfound));
				else refplayer.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.block));
				if (found && main.conf.blacklist && !blocked) { //запись статистики в файл игрока
					File f = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
					int uuidnar = -1;
					int ipnar = -1;
					if (f.isFile()) {
						configLoader loader = ut.cfload.getLoader();
						loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						ut.offLog();
						if (loader.isSet("ipnar")) ipnar = loader.getInt("ipnar");
						if (loader.isSet("uuidnar")) uuidnar = loader.getInt("uuidnar");
						ut.onLog();
					} else {
						File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/data");
						if (!fold.isDirectory()) fold.mkdirs();
					}
					if (uuidnar >= main.conf.narnum | ipnar >= main.conf.narnum) {
						configWriter writer = ut.cfwrite.getWriter();
						writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/data/" + refer.toLowerCase() + ".yml");
						writer.setOption("blocked", "true");
					}
				}
				if (main.conf.stat && found & !blocked) { //обновление общей статистики
					File st = new File(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
					int noref = 1;
					if (st.isFile()) {
						configLoader loader = ut.cfload.getLoader();
						loader.load(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
						ut.offLog();
						if (loader.isSet("noref")) noref += loader.getInt("noref");
						ut.onLog();
					} else {
						File fold = new File(ut.getPatch() + "/plugins/Vomine/ref/stat");
						if (!fold.isDirectory()) fold.mkdirs();
					}
					configWriter writer = ut.cfwrite.getWriter();
					writer.setConfig(ut.getPatch() + "/plugins/Vomine/ref/stat/all.yml");
					writer.setOption("noref", String.valueOf(noref));
				}
				if (main.conf.refmess) main.log.info(referal + " не был приглашен " + refer + " (ошибка или жульничество)");
				checkAch(refer); //проверка на достижения
			}
			ut.fdb.get().clear(); //очистка БД из памяти
		}
	}
	}
}